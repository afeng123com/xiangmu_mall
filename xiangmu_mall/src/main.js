import Vue from 'vue'
import App from './App.vue'
import router from './router/index';
import http from '@/api/config';
import store from '@/store/index';
//解决移动端300ms延迟
import fastClick from 'fastclick';
fastClick.attach(document.body);

//图片的懒加载
import vueLazyLoad from 'vue-lazyload';
Vue.use(vueLazyLoad,{
  error:'', //加载失败的图
  loading:require('@/assets/image/123.png') //加载中的默认图
  })
//给vue原型设置一个$HTTP来进行请求
Vue.prototype.$http = http;
Vue.prototype.$bus = new Vue();
import './assets/css/global.css'
import { Button } from 'vant';
import { Swipe, SwipeItem } from 'vant';
import { Grid, GridItem } from 'vant';
import { Tab, Tabs } from 'vant';
import { Col, Row } from 'vant';
import { Icon } from 'vant';
import { Tag } from 'vant';
import { GoodsAction, GoodsActionIcon, GoodsActionButton } from 'vant';
import { Toast } from 'vant';
import { SubmitBar } from 'vant';
import { Checkbox, CheckboxGroup } from 'vant';
import { Collapse, CollapseItem } from 'vant';

Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(SubmitBar);
Vue.use(Toast);
Vue.use(GoodsAction);
Vue.use(GoodsActionButton);
Vue.use(GoodsActionIcon);
Vue.use(Tag);
Vue.use(Col);
Vue.use(Row);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(Icon);
Vue.use(Button);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Grid);
Vue.use(GridItem);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
