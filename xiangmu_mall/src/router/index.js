import Vue from 'vue';
import router from 'vue-router';

Vue.use(router);

const pageHome = ()=>import('../views/pageHome/pageHome');
const pageList = ()=>import('../views/pageList/pageList');
const pageProfile = ()=>import('../views/pageProfile/pageProfile');
const pageShop = ()=>import('../views/pageShop/pageShop');
const homeGoodsDetail = ()=>import('../views/goodsDetail/homeGoodsDetail');
export default new router({
  routes: [
  	{
  		path: '',
  		redirect: '/pageHome'
  	},
  	{
  		path: '/pageHome',
  		component: pageHome
  	},
  	{
  		path: '/pageList',
  		component: pageList
  	},
  	{
  		path: '/pageProfile',
  		component: pageProfile
  	},
  	{
  		path: '/pageShop',
  		component: pageShop
	  },
	{
		path: '/goodsDetail',
		component: homeGoodsDetail
	  }
  ],
  mode: 'history'
})