export default{
    calcPrice(state) {
        //先过滤一下数组中的iconShow为true的数组
        let price = state.info.filter((item)=>{
            return item.iconShow === false;
        }).reduce((index, item) => {
            return item.lowNowPrice * item.count + index;
        }, 0);
        let allPrice = price * 100;
        return allPrice;
    },
    cartList(state){
        return state.info;
    }
}