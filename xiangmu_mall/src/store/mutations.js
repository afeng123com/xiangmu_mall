import Vue from 'vue';
export default{
    addList(state, val) {
        val.count = 1;
        state.info.push(val);
    },
    addCount(state,item){
        item.count++;
    },
    changeIconShow(state, val) {
        Vue.set(state.info[val],'iconShow',!state.info[val].iconShow);
        console.log(99,state.info);
    }
}